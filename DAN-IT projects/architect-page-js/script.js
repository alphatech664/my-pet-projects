$(".logo").after(
  $(`<ul class="section-url">
<li class="most-popular-url"><a href="#most-popular">Most Popular</a></li>
<li class="clients-url"><a href="#clients">Clients</a></li>
<li class="top-rated-url"><a href="#top-rated">Top Rated</a></li>
<li class="hot-news-url"><a href="#hot-news">Hot News</a></li>
</ul>`)
);

$(".section-url").css({
  display: "flex",
});

$(".section-url li")
  .css({
    "padding-right": "30px",
    "font-weight": "600",
  })
  .hover(
    function () {
      $(this).css({
        color: "#14b9d5",
        transition: "0.3s ease-in-out",
      });
    },
    function () {
      $(this).css({
        color: "white",
        transition: "0.3s ease-in-out",
      });
    }
  );

$('a[href^="#"]').click(function () {
  let anchor = $(this).attr("href");
  $("html, body").animate(
    {
      scrollTop: $(anchor).offset().top,
    },
    1000
  );
});

$("title").before(`<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">`);

$(".container").append($("<button>", { id: "button-up" }));
$("#button-up")
  .append(`<i class="fa fa-chevron-up"></i>`)
  .css({
    display: "none",
    position: "fixed",
    right: "20px",
    bottom: "100px",
    color: "#fff",
    "background-color": "#000",
    "text-align": "center",
    "font-size": "30px",
    padding: "15px",
    transition: ".3s",
  })
  .hover(
    function () {
      $("#button-up").css({
        cursor: "pointer",
        "background-color": "#14b9d5",
        transition: ".3s",
      });
    },
    function () {
      $("#button-up").css({
        cursor: "default",
        "background-color": "#000",
        transition: ".3s",
      });
    }
  );

$(document).ready(function () {
  const button = $("#button-up");
  $(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
      button.fadeIn();
    } else {
      button.fadeOut();
    }
  });
  button.on("click", function () {
    $("body, html").animate(
      {
        scrollTop: 0,
      },
      800
    );
    return false;
  });
});

// .nav-menu a {
//   transition: 0.3s ease-in-out;
// }
// .nav-menu a:hover {
//   color: #14b9d5;
//   transition: 0.3s ease-in-out;
// }

$.fn.extend({
  toggleText: function (a, b) {
    return this.text(this.text() == b ? a : b);
  },
});

$("#top-rated").append($("<button>", { text: "Свернуть", class: "toggle__button" }));
$(".toggle__button").click(function () {
  $(".top-rated-photos").slideToggle("slow");
  $(".toggle__button").toggleText("Свернуть", "Развернуть");
});
