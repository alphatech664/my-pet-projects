document.addEventListener('DOMContentLoaded', function () {
  const html = document.firstElementChild;
  const button = document.querySelector('button');

  const setTheme = (theme = 'light') => {
    html.setAttribute('color-scheme', theme);
    localStorage.setItem('Theme', `${theme}`);
  };

  button.addEventListener('click', () => {
    if (html.getAttribute('color-scheme') === 'light') {
      setTheme('dark');
    } else {
      setTheme('light');
    }
  });
  const theme = localStorage.getItem('Theme');
  setTheme(theme);
});

// document.addEventListener('DOMContentLoaded', function () {
//   const html = document.firstElementChild;
//   const switcher = document.querySelector('#theme-switcher');

//   const setTheme = (theme) => {
//     html.setAttribute('color-scheme', theme);
//     localStorage.setItem('Theme', `${theme}`);
//   };

//   switcher.addEventListener('input', (event) => {
//     setTheme(event.target.value);
//   });

//   const theme = localStorage.getItem('Theme');
//   setTheme(theme);
//   const input = document.getElementById(`${theme}`);
//   input.setAttribute('checked', '');
// });
