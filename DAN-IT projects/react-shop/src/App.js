import './style.scss';
import Modal from './components/Modal/Modal';
import React, { Component} from 'react';
import ProductList from './components/Products/ProductList';
import FavoritesList from './components/FavoriteProducts/FavoritesList';
import Header from './Header/Header';

class App extends Component {

  constructor(props){
    super(props)
    this.state = {
      currentModalWindow: {
          isModalShown: false,
          modalId: null,
          actionOnOk: null,
          paramsForAction: null
      },
      allProducts:{
        list: [],
        isLoaded: false,
      },
      favoriteProducts: {
        isOpened: false,
      },
      productsInCart: {
        isOpened: false 
      },
      isProductListLoaded: false,
      error: null,
    }}

  componentDidMount(){
    fetch('/Goods/goods.json')
    .then(response => response.json())
    .then(
      (result) => {this.setState({
        ...this.state,
        allProducts: {
          ...this.state.allProducts,
          list: result,
          isLoaded: true,
        },
      });
    },
      (error) => {this.setState({
        ...this.state,
        isProductListLoaded: true,
        error
      })}
    )
  }

  componentWillUnmount() {
    
  }

  toggleModal = (
    modalId=null, 
    actionOnOk=null,
    paramsForAction=null
    ) => {
  this.setState({
    ...this.state,
    currentModalWindow: {
      ...this.state.currentModalWindow,
      isModalShown: !this.state.currentModalWindow.isModalShown,
      modalId: (!this.state.currentModalWindow.isModalShown) ? modalId : "",
      actionOnOk: actionOnOk,
      paramsForAction: paramsForAction
    }
  })
}

  toggleProductInLocalStorageFavorite = (product) => {
    
    let favoriteProducts = localStorage.getItem('favoriteProducts');
    let favoriteProductsParsed = JSON.parse(favoriteProducts);
    if (!favoriteProductsParsed) favoriteProductsParsed = [];

    const isProductFavorite = favoriteProductsParsed.find(item => item.id === product.id);

    let isFavorite = false;
    if (!isProductFavorite) {
        isFavorite = true;
        favoriteProductsParsed = [...favoriteProductsParsed, product]
    } else {
        favoriteProductsParsed = favoriteProductsParsed.filter(item => item.id !== product.id)
    };
    
    this.setState({
      ...this.state,
      allProducts:{
        ...this.state.allProducts,
        list: [
          ...this.state.allProducts.list.map(item => item.id === product.id ? {...item, isFavorite: isFavorite} : item),
        ]
      },
    })

    favoriteProducts = JSON.stringify(favoriteProductsParsed);
    localStorage.setItem('favoriteProducts',favoriteProducts);    
    }

  toggleProductInLocalStorageCart = (product) => {
    
    let cartProducts = localStorage.getItem('cartProducts');
    let cartProductsParsed = JSON.parse(cartProducts);
    if (!cartProductsParsed) cartProductsParsed = [];

    const isProductInCart = cartProductsParsed.find(item => item.id === product.id);

    let isInCart = false
    if (!isProductInCart) {
        isInCart = true;
        cartProductsParsed = [...cartProductsParsed, product]
    } else {
        cartProductsParsed = cartProductsParsed.filter(item => item.id !== product.id)
    }
    
    this.setState({
      ...this.state,
      allProducts:{
        ...this.state.allProducts,
        list: [
          ...this.state.allProducts.list.map(item => item.id === product.id ? {...item, isInCart: isInCart} : item),
        ]
      },
    })

    cartProducts = JSON.stringify(cartProductsParsed);
    localStorage.setItem('cartProducts',cartProducts);    
    }


  toggleCartShowing = (show=false) => {
    
    this.setState({
      ...this.state,
      productsInCart:{
        ...this.state.productsInCart,
        isOpened: show
      }
    })
  }


  render() {
    return (
      <>
      {this.state.currentModalWindow.isModalShown &&
      <Modal 
      {...this.state.currentModalWindow}
      toggleModal={this.toggleModal}
      />
      }
      <Header 
      toggleProductInLocalStorageCart={this.toggleProductInLocalStorageCart}
      toggleCartShowing={this.toggleCartShowing}
      isCartOpened={this.state.productsInCart.isOpened}/>

      {this.state.allProducts.isLoaded && this.state.allProducts.list !== null && 
        <ProductList
          products={this.state.allProducts.list}
          toggleModal={this.toggleModal}
          toggleProductInLocalStorageFavorite={this.toggleProductInLocalStorageFavorite}
          toggleProductInLocalStorageCart={this.toggleProductInLocalStorageCart}
        />
      }
      {this.state.favoriteProducts.isOpened && 
        <FavoritesList 
          toggleProductInLocalStorageFavorite={this.toggleProductInLocalStorageFavorite}
        />
      }
    </>
  );
  }
}

export default App;