import React, {Component} from 'react';
import './ButtonGenerateModal.scss';

export default class ButtonGenerateModal extends Component {

    render(){

        const {btnText, dataModalId, actionOnOk, toggleModal, paramsForAction, disabled} = this.props;
          
        return(
            <button disabled={disabled} className="OpenModalBtn" onClick={()=>toggleModal(dataModalId,actionOnOk,paramsForAction)}>{btnText}</button>
        );
    }
}