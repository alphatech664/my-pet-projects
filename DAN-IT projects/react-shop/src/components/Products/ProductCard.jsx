import React, { Component } from "react";
import ButtonGenerateModal from "../Button/ButtonGenerateModal";
import './ProductCard.scss'
import Modal from "../Modal/Modal.jsx";
import LikeButtonIcon from "../icons/LikeButtonIcon";

export default class ProductCard extends Component{



    render(){

        const {product, toggleModal, toggleProductInLocalStorageFavorite, toggleProductInLocalStorageCart} = this.props;

        const favoriteLocalStorage = JSON.parse(localStorage.getItem('favoriteProducts'));
        const cartLocalStorage = JSON.parse(localStorage.getItem('cartProducts'));
        const isInFavoriteLocalStorage = favoriteLocalStorage ? favoriteLocalStorage.find(item => item.id === product.id) : false;
        const isInCartLocalStorage = cartLocalStorage ? cartLocalStorage.find(item => item.id === product.id) : false;

        return(
            <>
                <li>
                    <div className="product">
                        <img className="product__image" src={product.pictureUrl} alt={product.name}/>
                        <div className="product__info">
                            <div> 
                                <p className="product__name">{product.name}</p>
                                <span onClick={()=>toggleProductInLocalStorageFavorite(product)}>
                                    <LikeButtonIcon fill={(isInFavoriteLocalStorage || product.isFavorite) ? "red" : ''}/>
                                </span>
                            </div>
                            <p className="product__description-color">{product.color}</p>
                            <div>
                            <p className="product__price">$ {product.price}</p>
                            {<ButtonGenerateModal
                                dataModalId='modalID3'
                                btnText={product.isInCart || isInCartLocalStorage ? "Already in Cart" : "Add to Cart"}
                                className={product.isInCart || isInCartLocalStorage ? "product__btnAddToCart" : "product__btnAddToCart product__btnAddToCart-disabled"}
                                disabled = {product.isInCart || isInCartLocalStorage}
                                toggleModal={toggleModal}
                                actionOnOk={toggleProductInLocalStorageCart}
                                paramsForAction={product}
                            />}
                            </div>
                        </div>
                    </div>
                </li>
            </>
        )
    }
}