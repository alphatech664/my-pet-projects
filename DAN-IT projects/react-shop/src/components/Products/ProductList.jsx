import React, {Component} from "react";
import ProductCard from "./ProductCard";

export default class ProductList extends Component {

    render(){
        
        const {products,toggleModal,toggleProductInLocalStorageFavorite,toggleProductInLocalStorageCart} = this.props;
        return(
            <>
         
                <ul className="products-block">
                    {products.map((product) => {
                        return (
                            <ProductCard 
                            key={product.id+'inFullList'} 
                            product={product}
                            toggleModal={toggleModal}
                            toggleProductInLocalStorageFavorite={toggleProductInLocalStorageFavorite}
                            toggleProductInLocalStorageCart={toggleProductInLocalStorageCart}
                            /> 
                        )
                    })}
                </ul>
            
            </>
        )
    }
}