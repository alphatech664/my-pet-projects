import React, {Component} from 'react';
import './Modal.scss'
import ModalWindowDeclarations from './ModalWindowDeclarations.jsx';

export default class Modal extends Component {

    proceedAndClose = () => {
        this.props.actionOnOk(this.props.paramsForAction)
        this.props.toggleModal()
    }

    render(){

        const {modalId, toggleModal} = this.props;

        const allModals = new ModalWindowDeclarations();
        const {title, description, closeBtn, backgroundColor} = allModals.findWindowById(modalId);
        const style = {
            backgroundColor: backgroundColor
        }

        return(
        <>
        <div className='blackout' onClick={toggleModal}></div>
           <div className='modal' style={style}>
                <div className='modalHeader'>
                <h5>{title}</h5>
                {closeBtn && <button onClick={toggleModal} className='closeBtn'>X</button>}
                </div>
                <p className='modalContent'>{description}</p>
                <div className='actionButtons'>
                    <button onClick={() => this.proceedAndClose()}>Ok</button>
                    <button onClick={toggleModal}>Cancel</button>
                </div>
           </div>
        </>
        );
    }

}