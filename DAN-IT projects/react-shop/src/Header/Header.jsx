import React, {Component} from "react";
import HeadPanel from "./HeadPanel";
import NavigationPanel from "./NavigationPanel";
import './Header.scss'

export default class Header extends Component {
    render(){
        return(
            <>
                <HeadPanel
                toggleProductInLocalStorageCart={this.props.toggleProductInLocalStorageCart}
                toggleCartShowing={this.props.toggleCartShowing}
                isCartOpened = {this.props.isCartOpened}
                />
                <NavigationPanel/>
            </>
        )
    }
}