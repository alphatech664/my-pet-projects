import { Component } from "react";

export default class UsersCredentials extends Component{
    render(){
        return(
            <>
                 <div className="usersCredentials">
                    <a href="#">Login</a>
                    / 
                    <a href="#">Register</a>
                </div>
            </>
        )
    }
}