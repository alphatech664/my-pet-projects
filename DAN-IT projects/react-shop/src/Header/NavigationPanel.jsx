import React, {Component} from "react";


export default class NavigationPanel extends Component {

    render(){

        let favoriteProducts = JSON.parse(localStorage.getItem('favoriteProducts'));
        favoriteProducts = favoriteProducts ? favoriteProducts : []

        return(
            <>
                <div className="navigationPanel container">
                    <div className="navigationPanel__mainLogo">
                        <img src="https://pixophone.com/image/cache/catalog/LOGO/%D1%81%D0%B0%D0%B9%D1%82-19-220x80.jpg"/>
                    </div>
                    <ul className="navigationPanel__nav">
                        <li className="navigationPanel__nav-item"><a href="#">Home</a></li>
                        <li className="navigationPanel__nav-item"><a href="#">Cart</a></li>
                        <li className="navigationPanel__nav-item"><a href="#">Favourite ({favoriteProducts.length})</a></li>
                    </ul>
                </div>
            </>
        )
    }
}