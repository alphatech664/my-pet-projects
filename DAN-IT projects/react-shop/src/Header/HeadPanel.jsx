import React, {Component} from "react";
import CartItem from "./CartItem";
import UsersCredentials from "./UsersCredentials";

export default class HeadPanel extends Component{
    render(){

        
        return(
            <>
                <div className="headPanel">
                    <div className="container">

                        <div className="userInfo">
                            <UsersCredentials/>
                            <CartItem
                            
                            isCartOpened={this.props.isCartOpened}
                            toggleProductInLocalStorageCart={this.props.toggleProductInLocalStorageCart}
                            toggleCartShowing={this.props.toggleCartShowing}
                            />
                        </div>
                    </div>
                </div>
                
            </>
        )
    }
}